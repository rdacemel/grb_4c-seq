# GRB_4C-seq contact calling

Repository with the code used to call interactions from 4C-seq experiments in the [Touceda-Suárez et al. 2019](https://www.biorxiv.org/content/10.1101/776369v1) paper. The calling is based in the strategy proposed in [de Wit et al. 2015](https://doi.org/10.1016/j.molcel.2015.09.023) with minor adjustments. 

## Requirements

The script is written in R and depends on the following R packages:

* [isotone](https://cran.r-project.org/web/packages/isotone/index.html)
* [GenomicRanges](https://bioconductor.org/packages/release/bioc/html/GenomicRanges.html)
* [zoo](https://cran.r-project.org/web/packages/zoo/index.html)
* [optparse](https://cran.r-project.org/web/packages/optparse/index.html)

## Algorithm rationale

The idea behind the interaction caller is fitting two monotonic regressions, one at each side of the viewpoint, and identifying those fragments that robustly show more interactions than the expected according to the regression model. In order to do that we subsample the reads (by default we only keep 10000 reads per experiment, which can be changed with the --subs_nreads flag) several times (by default 1000 times, which can be changed with the --subs_events flag) and keep only those fragments that are 1.2 times (--iso_thresh) above the expected signal 
in more than 99.5% of the cases (this can also be changed with the --subs_freq flag). In order to avoid PCR duplicate bias (that cannot be sorted out in regular 4C-seq experiments) we filter out interactions in which only one fragment is covered with reads, also those where less than the 20% of the interacting fragments are covered (due to the smoothing window procedure there are empty fragments that can have high scores because of neighboring fragments, this parameter can be changed with the --cov_thresh flag). Interactions smaller than 2kb are also filtered out (see --legth_thesh flag). In the next section we explain how to run the script. 

## Running the script

This is the help message that we can find upon calling:

```bash
Rscript scritps/peak4C.R -h
```

```
Options:
	--bg=BG
		input 4C-seq bedgraph, not smoothed

	--chrom=CHROM
		chromosome of the viewpoint

	--coord=COORD
		coordinates of the viewpoint

	--name=NAME
		name of the experiment, defaults to unknown

	--outdir=OUTDIR
		prefix of the results folder, defaults to ./results

	--lWindow=LWINDOW
		landscape window, defaults to +-1Mb

	--sWindow=SWINDOW
		smoothing window, defaults to 29 fragments

	--iso_thresh=ISO_THRESH
		ratio signal/threshold needed for calling an interaction, defaults to 1.2

	--subs_freq=SUBS_FREQ
		the interaction should be called more frequently than this threshold in the subsampling, defaults to 0.995

	--subs_nreads=SUBS_NREADS
		number of reads taken in each subsampling event, defaults to 1000

	--subs_events=SUBS_EVENTS
		Number of time the subsampling is performed, defaults to 1000 times

	--cov_thresh=COV_THRESH
		threshold for the proportion of fragments that need to be covered in an interaction to be consider valid, defaults to 20% (0.2)

	--length_thresh=LENGTH_THRESH
		Minimum interaction length, defaults to 2000bp

	--set_seed=SEED
		seed for the replicated subsampling events, defaults to 0

	-h, --help
		Show this help message and exit
```

Apart from the different parameters that can be adjusted and were commented on before, there are 4 basic inputs that the code need. One of them is a bedgraph file containing of the 4C-seq experiment containing the raw counts of reads per restriction fragment (should be provided after the -bg flag, it is possible to find examples under the [data folder](data/fragcount_bg)). Then the program also needs to know the chromosome and the genomic coordinate of the viewpoint (after the --chrom and --coord flags). These three options are mandatory but is it advisable to provide also a name for the experiment to name the output files (--name). Below is a standard call to the program, like the ones performed to analyze the experiments in the [Touceda-Suárez et al. 2019](https://www.biorxiv.org/content/10.1101/776369v1) paper. All of them are gathered in [this bash script](scripts/run_all.sh). 

```bash
Rscript scritps/peak4C.R --bg data/fragcount_bg/isl2a_24hpf.fragscounts --chrom chr25 --coord 33738038 --name isl2a_24hpf  > logs/isl2a_24hpf.err 2>&1 &
```

## Results explained

The most obvious result file is just a bedfile containing the regions that are significantly interacting with our promoter of interest, for instance in the case of the isl2a promoter we have the [isl2a_24hpf_filtered-interactions.bed file](results/isl2a_24hpf_filtered-interactions.bed). In addition to that, the script also generate a [pdf file](results/isl2a_24hpf.pdf) with a plot that shows the fitted monotonic regression together with the 4C-seq signal and the called interactions as blue rectangles. In addition, three extra intermediate files are generated that could be useful. 

One is a [fraginfo.tsv](results/isl2a_24hpf_fraginfo.tsv) file wich contains the following information of the restriction fragments contained within the landscape window (by default +- 1Mb around the viewpoint): chrom-start-end-score-iso-subscore-valid. The first three are self explanatory. The score is the smoothened signal obtained from running a smoothing window algorithm on the raw data provided (by default it uses a 29 fragment window, which can be changed with the --lWindow parameter). Iso is the value of the monotonic regression in that particular fragment. Subscore is the amount of times the score was higher than the monotonic regression in the subsampling events. Valid is a boolean column of whether the subscore column is over the required threshold (--subs_freq). 

The other two are the [merged-interactions.tsv](results/isl2a_24hpf_merged-interactions.tsv) and the [filtered-interactions.tsv](results/isl2a_24hpf_filtered-interactions.tsv) that contain the following information: chrom-start-end-width-strand-revmap-nfrags-covfrags-cov. In the first file there are all the merged interactions that pass the --subs_freq filter. In the second, the additional filters are applied. The most interesting columns are the width (with is the width of the interaction that need to be higher than 2kb as specified in --length_thresh), the nfrags (which is the number of fragments merged for this interaction), the covfrags (that are the number of fragments with coverage, a number that should be higher than one) and cov that is the proportion of the fragments merged with coverage (that should be above 0.2, as specified with the --cov_thresh flag).

# Spearmann correlation of gene expression between GRB ohonologs from mouse single cell experiments

We include in the [spearmann_sc-mm9](spearmann_sc-mm9) folder the R code used to calculate the expression correlation between GRB ohnologs using mouse single-cell RNA-seq experiments from [Cao et al. 2019](https://www.nature.com/articles/s41586-019-0969-x). 
