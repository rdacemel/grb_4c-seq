set -o xtrace
srun Rscript scripts/peak4C.R --bg data/fragcount_bg/Otx1_E95.fragscounts --chrom chr11 --coord 22003084 --name Otx1_E95 > logs/Otx1_E95.err 2>&1 &
srun Rscript scripts/peak4C.R --bg data/fragcount_bg/Otx2_E95.fragscounts --chrom chr14 --coord 48663679 --name Otx2_E95  > logs/Otx2_E95.err 2>&1 &
srun Rscript scripts/peak4C.R --bg data/fragcount_bg/Isl1_E95.fragscounts --chrom chr13 --coord 116309099 --name Isl1_E95  > logs/Isl1_E95.err 2>&1 &
srun Rscript scripts/peak4C.R --bg data/fragcount_bg/Isl2_E95.fragscounts --chrom chr9 --coord 55539931 --name Isl2_E95  > logs/Isl2_E95.err 2>&1 &

