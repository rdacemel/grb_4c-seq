srun Rscript scritps/peak4C.R --bg data/fragcount_bg/isl2a_24hpf.fragscounts --chrom chr25 --coord 33738038 --name isl2a_24hpf  > logs/isl2a_24hpf.err 2>&1 &
srun Rscript scritps/peak4C.R --bg data/fragcount_bg/otx1a_24hpf.fragscounts --chrom chr1 --coord 51429633 --name otx1a_24hpf  > logs/otx1a_24hpf.err 2>&1 &
srun Rscript scritps/peak4C.R --bg data/fragcount_bg/otx1b_24hpf.fragscounts --chrom chr17 --coord 24145586 --name otx1b_24hpf  > logs/otx1b_24hpf.err 2>&1 &
srun Rscript scritps/peak4C.R --bg data/fragcount_bg/otx2_24hpf.fragscounts --chrom chr17 --coord 44293937 --name otx2_24hpf  > logs/otx2_24hpf.err 2>&1 &
srun Rscript scritps/peak4C.R --bg data/fragcount_bg/hey2_24hpf.fragscounts --chrom chr20 --coord 39623363 --name hey2_24hpf  > logs/hey2_24hpf.err 2>&1 &
srun Rscript scritps/peak4C.R --bg data/fragcount_bg/isl1_24hpf.fragscounts --chrom chr5 --coord 42360584 --name isl1_24hpf  > logs/isl1_24hpf.err 2>&1 &
srun Rscript scritps/peak4C.R --bg data/fragcount_bg/Isl_15hpf.fragscounts --chrom Sc0000155 --coord 622237 --name Isl_15hpf  > logs/Isl_15hpf.err 2>&1 &
srun Rscript scritps/peak4C.R --bg data/fragcount_bg/Otx_15hpf.fragscounts --chrom Sc0000240 --coord 63514 --name Otx_15hpf  > logs/Otx_15hpf.err 2>&1 &
